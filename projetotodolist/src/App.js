import React, { Component } from 'react';
import TodoList from './components/TodoList';

class App extends Component{
//todolist
  render(){
    return(
      <div>
        <TodoList/>
      </div>
    );
  }
}

export default App;