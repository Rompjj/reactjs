import React, { Component } from 'react';
import TodoItem from './TodoItem';
import './estilo.css';

class TodoList extends Component{

  constructor(props){
    super(props);
    this.state = {
        nomes: '',
        items:[]
    }
    this.addItem = this.addItem.bind(this);
    this.deleteItem = this.deleteItem.bind(this);

  }

  addItem(evento){
    let state = this.state;

    if(this.nomesInput.value != ''){
        let newItem = {
            text: this.nomesInput.value,
            key: Date.now()
        };
        this.setState({items: [...state.items,newItem]});
    }else{
        alert("Preencha corretamente o campo.")
    }
    //console.log(this.state.items);
    this.setState({nomes: ''});
    evento.preventDefault();
  }

  deleteItem(key){
    let filtro = this.state.items.filter((item)=>{
        return(item.key != key);
    })
    this.setState({items: filtro});
  }

  render(){
    return(
      <div className="bloco">
        <form onSubmit={this.addItem}>
        <input type='text' placeholder='Nome para Lista?' name='nomes' value={this.state.nomes} onChange={(evento)=> this.setState({nomes: evento.target.value})}
        ref={(evento)=> this.nomesInput = evento} />
        
        <button type='sumit'>Adicionar</button>
        </form>

        <TodoItem lista={this.state.items} delete={this.deleteItem}/>
      
      </div>
    );
  }
}

export default TodoList;