import React, { Component } from 'react';

class TodoItem extends Component{
    constructor(props){
        super(props);
        this.state = {};
            
        this.delete = this.delete.bind(this);
    }

    delete(key){
        this.props.delete(key);
    }

  render(){
    return(
      <div>
        <table>
            
                {this.props.lista.map((item)=> {
                    return(
                    <fieldset>
                        <tr>
                            <td onClick={()=> this.delete(item.key)} key={item.key}>
                                {item.text}
                            </td>
                        </tr>
                    </fieldset>
                    );
                })}
        </table>
      </div>
    );
  }
}

export default TodoItem;