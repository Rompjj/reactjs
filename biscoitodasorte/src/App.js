import React, { Component } from 'react';
import './estilo.css'

class App extends Component{
  constructor(props){
    super(props);
    this.state = {
      textoFrase:''
    };

    this.quebraBiscoito = this.quebraBiscoito.bind(this);
    this.limpar = this.limpar.bind(this);

    this.frases = ['Siga os bons e aprenda com eles.', 'O bom-senso vale mais do que muito conhecimento.', 
    'O riso é a menor distância entre duas pessoas.', 
    'Deixe de lado as preocupações e seja feliz.',
    'Realize o óbvio, pense no improvável e conquiste o impossível.',
    'Acredite em milagres, mas não dependa deles.',
    'A maior barreira para o sucesso é o medo do fracasso.',
    'Seja alguem, depois queira alguem.'];
  }

  /* Math -> funcao matematica/ floor -> arredonda pra inteiro/ random -> gera numero aleatorio com decimais de 0 ao maximo de frases/ length -> cconta o numero de string no objeto array */
  quebraBiscoito(){
    let state = this.state;
    let numeroAleatorio = Math.floor(Math.random() * this.frases.length);
    state.textoFrase = this.frases[numeroAleatorio];
    this.setState(state);
  }

  limpar(){
   let state = this.state;
   state.textoFrase = '';
   this.setState(state);
  }

  render(){
    return(
      <div className='bloco'>
        <img src={require('./imagens/biscoito.png')} className='img' />
        <div className='sessao'>
        <Botao nome="Quebrar" acaoBut={this.quebraBiscoito}/>
        <button onClick={this.limpar} className='but'>Limpar</button>
        </div>
        <h3 className='textoFrase'>{this.state.textoFrase}</h3>
      </div>
    );
  }
}

class Botao extends Component{
  render(){
    return(
      <div>
        <button onClick={this.props.acaoBut} className='but'>{this.props.nome}</button>
      </div>
    );
  }
}

export default App;