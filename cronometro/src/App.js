import React, { Component } from 'react';
import './estilo.css'

class App extends Component{
  constructor(props){
    super(props);
    this.state = {
      numero: 0,
      iniciar: 'Iniciar'
    };
    this.timer = null; //inicio meu timer no null
    this.iniciar = this.iniciar.bind(this); //botao iniciar
    this.zerar = this.zerar.bind(this); //botao zerar

  }
 //setInterval() -> metodo funcao js, repete a execução da função continuamente defina por tempo
 // clearInterval()->método interrompe as execuções da função especificada no método setInterval 

 iniciar(){
    let state = this.state; //sempre

    if(this.timer !== null){
      clearInterval(this.timer); //pausa o metodo setInterval() do timer
      this.timer = null; //atribui null
      state.iniciar = 'Iniciar'; //escreve no botao iniciar iniciar
    }else{
      this.timer = setInterval(()=>{  
                    let state = this.state;  
                    state.numero +=0.1;   //incrementa 0.1 no timer
                    this.setState(state);  //atualiza o state
                    },100); //faz durante 100 ms
      state.iniciar = "Pausar"; //escreve pausar no botao iniciar
    }

    this.setState(state);  //atualiza o state
  }

  zerar(){
    if(this.timer !== null){
      clearInterval(this.timer);
      this.timer = null;
    }

    let state = this.state;
    state.numero = 0; //escreve 0 no 
    state.iniciar = "Iniciar";

    this.setState(state);

  }
  /*toFixed() retorna uma string, com o número escrito com casas decimais*/
  render(){
    return(
      <div className="bloco">
        <img src={require('./imagens/cronometro.png')} className="img"/>
          <a className="timer">{this.state.numero.toFixed(1)}</a>
        <div className="areaBut">
    <a className="botao" onClick={this.iniciar}>{this.state.iniciar}</a>
          <a className="botao" onClick={this.zerar}>Zerar</a>
        </div>
      </div>
    );
  }

}
export default App;
