import React from 'react';
import { Link } from 'react-router-dom';
import Logo from '../../assets/imagens/Logo.png';
//import ButtonLink from './components/ButtonLink';
import '../Menu/Menu.css';
import Button from '../Button';

function Menu(){
    return(
        <header>
            <nav className="Menu">
                <Link to="/">
                    <img className="Logo" src={Logo} alt="rodflix logo"/>
                </Link>
                
                <Button as={Link} className="ButtonLink" to="/cadastro/Video">
                    Novo video
                </Button>
            </nav>
        </header>
    );
}

export default Menu;