/* eslint-disable linebreak-style */
import React from 'react';
import { Link } from 'react-router-dom';
import PageDefault from '../../../componentes/PageDefault';

// to = href
function CadastroVideo() {
  return (
    <PageDefault>
      <h1>Cadastro de video.</h1>

      <Link to="/cadastro/categoria">
        Cadastrar Categoria
      </Link>
    </PageDefault>
  );
}

export default CadastroVideo;
