import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import App from './pages/Home/App';
import CadastroVideo from './pages/cadastro/Video';
import CadastroCategoria from './pages/cadastro/Categoria';

// importacao da biblioteca de routeamento
// eslint-disable-next-line max-len
/* Switch -> como se fosse if, Router -> o que vai ser renderizado, path ->  o que e digitado na area de navegador */
const Pagina404 = () => (
  <div className="erro">
    Erro 404.
  </div>
);

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route path="/" component={App} exact />
      <Route path="/Cadastro/Video" component={CadastroVideo} />
      <Route path="/Cadastro/categoria" component={CadastroCategoria} />
      <Route component={Pagina404} />
    </Switch>
  </BrowserRouter>,
  document.getElementById('root'),
);
